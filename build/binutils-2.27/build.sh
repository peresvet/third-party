#! /bin/sh
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the Binutils package.
##
## Author:
##
##     Evan Green 18-Jan-2017
##
## Environment:
##
##     Build
##
## Forked for building Peresvet Binutils

. ../build_common.sh

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure-tools)
    export AR='ar'
    sh ${SOURCE_DIRECTORY}/configure --target="$TARGET" \
                                     --disable-nls \
                                     --disable-werror \
                                     --prefix="/" \
                                     --with-sysroot="/" \
                                     --disable-shared \
                                     --enable-multilib \
                                     CFLAGS="$CFLAGS"

    ;;

  configure)
    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --disable-nls \
                                     --disable-werror \
                                     --prefix="/usr" \
                                     --with-sysroot="/" \
                                     --disable-shared \
                                     --enable-multilib \
                                     CFLAGS="$CFLAGS"

    ;;

  build-tools)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY/"

    ##
    ## Create symbolic links for the Makefile-based OS build.
    ##

    if [ "$BUILD_OS" = "peresvet" ]; then
        cd "$OUTPUT_DIRECTORY/bin"
        for app in ar as ld objcopy strip; do
            ln -sf $app $TARGET-$app
        done
    fi

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY/"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

