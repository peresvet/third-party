##
## Copyright (c) 2013 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     guessos.sh
##
## Abstract:
##
##     This script attempts to figure out which OS the build machine is running
##     on.
##
## Author:
##
##     Evan Green 28-Oct-2013
##
## Environment:
##
##     Build with POSIX tools.
##
## Adopted under Peresvet OS by:
##
##     Andrei Zaikin 06-Jul-2017
##

set -e

SYSTEM_NAME=`uname -s`
case $SYSTEM_NAME in
  MINGW32*)
    echo "win32" ;;

  Peresvet)
    echo "peresvet" ;;

  Linux)
    echo "linux" ;;

  Darwin)
    echo "macos" ;;

  CYGWIN*)
    echo "cygwin" ;;

  FreeBSD*)
    echo "freebsd" ;;

  *)
    exit 1;;
esac

exit 0
