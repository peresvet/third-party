##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     minimal_config.sh
##
## Abstract:
##
##     This script performs a minimal amount of system configuration needed
##     after a basic offline install.
##
## Author:
##
##     Evan Green 5-Dec-2016
##
## Environment:
##
##     Build with POSIX tools.
##
## Adopted under Peresvet OS by:
##
##     Andrei Zaikin 06-Jul-2017
##

set -e

if [ -z "$SRCROOT" ] || [ -z "$ARCH" ] || [ -z "DEBUG" ]; then
    echo "Error: SRCROOT, ARCH, and DEBUG are required."
    exit 1
fi

##
## Dropbear never got a chance to add its init scripts.
##

export SYSROOT=$SRCROOT/$ARCH$VARIANT$DEBUG/bin/apps
sh $SRCROOT/os/apps/posix/update-rc.d dropbear defaults

##
## If the user doesn't have an authorized_keys file, prompt them to create one.
##

if ! [ -s $SYSROOT/root/.ssh/authorized_keys ]; then
    printf "You have no authorized SSH keys. Should I create one now [y]? "
    read answer
    [ -z "$answer" ] && answer=yes
    if echo "$answer" | grep -qi '^y'; then
        KEYGEN=`which ssh-keygen || which ssh-keygen.exe || true`

        ##
        ## If no ssh-keygen was found but there's one in $SRCROOT/git, use that.
        ##

        if ! [ -x "$KEYGEN" ]; then
            [ -x "$SRCROOT/git/usr/bin/ssh-keygen" ] && \
                 KEYGEN="$SRCROOT/git/usr/bin/ssh-keygen"
        fi

        if ! [ -x "$KEYGEN" ]; then
            echo "Error: ssh-keygen not found. You'll have to do it manually."
            exit 1
        fi

        mkdir -p "$SYSROOT/root/.ssh"
        chmod 700 "$SYSROOT/root/.ssh"
        cmd="$KEYGEN -t rsa -b 4096 -N '' -f $SYSROOT/root/.ssh/id_rsa"
        echo $cmd
        eval $cmd # to properly pass arguments. -N '' in particular
        cat "$SYSROOT/root/.ssh/id_rsa.pub" >> \
            "$SYSROOT/root/.ssh/authorized_keys"

        chmod 600 "$SYSROOT/root/.ssh/id_rsa"
        chmod 600 "$SYSROOT/root/.ssh/authorized_keys"
        echo "Your key is at $SYSROOT/root/.ssh/id_rsa"

    else
        echo "Okay, not adding any authorized keys."
        echo "Remember to add one to $SYSROOT/root/.ssh/authorized_keys"
    fi
fi

##
## Change permissions on the authorized_keys file if needed.
##

exit 0
