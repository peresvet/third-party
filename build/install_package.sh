##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     install_package.sh
##
## Abstract:
##
##     This script performs an offline install of the given package
##     (in the form package-version) to the default apps directory.
##
## Author:
##
##     Evan Green 5-Dec-2016
##
## Environment:
##
##     Build with POSIX tools.
##
## Adopted under Peresvet OS by:
##
##     Andrei Zaikin 06-Jul-2017
##

set -e

IPK=$1
DEST=$2

if [ -z "$SRCROOT" ]; then
    echo "Error: SRCROOT is required."
    exit 1
fi

check_environment () {
    if [ -z "$DEBUG" ]; then
        echo "Error: DEBUG is required."
        exit 1
    fi

    if [ -z "$ARCH" ]; then
        echo "Error: ARCH is required."
        exit 1
    fi
}

##
## If the first argument is not a path to the IPK, attempt to convert it into
## one.
##

if ! [ -r "$IPK" ]; then
    check_environment
    case "$ARCH$VARIANT" in
    x86q) parch=i586 ;;
    x86) parch=i686 ;;
    armv7) parch=armv7 ;;
    armv6) parch=armv6 ;;
    esac

    if [ -z "$parch" ]; then
        echo "Error: Unknown architecture $ARCH."
        exit 1
    fi

    set `echo $IPK | sed 's/\(.*\)-/\1 /'`
    if [ -z "$1" -o -z "$2" ]; then
        echo "Error: Invalid package specification $IPK."
        exit 1
    fi

    PKGROOT="$SRCROOT/$ARCH$VARIANT$DEBUG/bin/packages"
    IPK="$PKGROOT/$1_$2_peresvet-$parch.ipk"
    if ! [ -r "$IPK" ]; then
        IPK2="$PKGROOT/$1_$2_peresvet-all.ipk"
        if [ -r "$PKGROOT/$IPK" ]; then
            IPK="$IPK2"
        fi
    fi

    if ! [ -r "$IPK" ]; then
        echo "Failed to find ipk $IPK."
        if [ "$IPK2" ]; then
            echo "Also tried $IPK2."
        fi

        exit 2
    fi
fi

##
## If no destination is set, use the default apps directory.
##

if [ -z "$DEST" ]; then
    check_environment
    DEST="$SRCROOT/$ARCH$VARIANT$DEBUG/bin/apps"
fi

mkdir -p "$DEST"
sh $SRCROOT/third-party/build/opkg-utils/opkg-extract-data "$IPK" "$DEST"
exit 0
