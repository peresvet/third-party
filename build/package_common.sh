##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package_common.sh <package_dir>
##
## Abstract:
##
##     This script performs actions common to all package.sh scripts. It is
##     meant to be sourced directly into each package generation script.
##
## Author:
##
##     Evan Green 11-Feb-2015
##
## Environment:
##
##     Build
##
## Adopted under Peresvet OS by:
##
##     Andrei Zaikin 06-Jul-2017
##

set -e

OPKG_UTILS="$SRCROOT/third-party/build/opkg-utils"
OPKG_BUILD="$OPKG_UTILS/opkg-build"

##
## Grab the arguments.
##

BUILD_DIRECTORY="$1"
PACKAGE_DIRECTORY="$2"

if test -z "$PKGOUT"; then
    echo "$0: Error: PKGOUT must be specified."
    exit 1
fi

if ! test -d "$BUILD_DIRECTORY"; then
  echo "$0: Error: Build directory $BUILD_DIRECTORY must exist."
  exit 1
fi

mkdir -p "$PKGOUT"
mkdir -p "$PACKAGE_DIRECTORY/CONTROL"
if ! test -d "$PACKAGE_DIRECTORY"; then
  echo "$0: Error: Package directory $PACKAGE_DIRECTORY must exist."
  exit 1
fi

case $ARCH in
x86)
    PACKAGE_ARCH=peresvet-i686
    if test "x$VARIANT" = "xq"; then
        PACKAGE_ARCH=peresvet-i586
    fi
    ;;

armv6)
    PACKAGE_ARCH=peresvet-armv6
    ;;

armv7)
    PACKAGE_ARCH=peresvet-armv7
    ;;

esac

##
## This function creates the ipk package. The first argument is the package
## directory. The next argument is the output directory.
##

create_package () {
    for file in control preinst postinst prerm postrm conffiles; do
        if [ -f "$1/CONTROL/$file" ]; then
            trim_cr "$1/CONTROL/$file"
        fi
    done
    sh "$OPKG_BUILD" $extra_args $@
    cp -v $2/*.ipk "$PKGOUT"
}

##
## This function removes carriage returns from a file.
##

trim_cr () {
    cp "$1" "$1.trim"
    cat "$1.trim" | tr -d '\r' > "$1"
    rm "$1.trim"
}

##
## This function computes the total size of the given file or directory and all
## of its contents. It is rounded up to the nearest kilobyte, and the result
## is returned in bytes. (Note: The size starts at one so that expr doesn't
## return a failure code if it ends up with a result of 0).
##

compute_size () {
    local size lines line
    size=1
    lines=`ls -lR $@ | sed -n 's/[^ ]* *[^ ]* *[^ ]* *[^ ]* *\([0123456789]*\).*/\1/p'`
    for line in $lines; do
        size=`expr $size + $line`
    done
    expr \( $size + 1023 \) / 1024 \* 1024
}

