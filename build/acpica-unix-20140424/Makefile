################################################################################
#
#   Copyright (c) 2013 Minoca Corp. All Rights Reserved.
#
#   Module Name:
#
#       acpica-unix-20140424
#
#   Abstract:
#
#       This makefile is responsible for building the iASL compiler package.
#
#   Author:
#
#       Evan Green 2-May-2014
#
#   Environment:
#
#       Build
#
################################################################################

include ../common.mk

PACKAGE := acpica-unix-20140424
PATCHED_SOURCE_DIRECTORY := $(OBJROOT)/$(PACKAGE).src
SOURCE_TARBALL := $(ROOT)/third-party/src/$(PACKAGE).tar.gz
ORIGINAL_SOURCE_DIRECTORY := $(OBJROOT)/$(PACKAGE).orig

TOOLBUILDROOT := $(OBJROOT)/$(PACKAGE).tool
BUILDROOT := $(OBJROOT)/$(PACKAGE).build
BINROOT := $(BUILDROOT)/build.out
PKGROOT := $(BUILDROOT)/build.pkg

DIFF_FILE := $(CURDIR)/$(PACKAGE).diff

.PHONY: all clean tools recreate-diff package

all: $(BUILDROOT)/Makefile
	mkdir -p $(BINROOT)/bin/
	@echo Building $(PACKAGE)
	cd $(BUILDROOT) && \
	    TMPDIR=$$PWD $(MAKE) CC="$(TARGET)-gcc" \
	         CWARNINGFLAGS="-Wall -g -fno-omit-frame-pointer"
	cp -Rp $(BUILDROOT)/generate/unix/bin/ $(BINROOT)/
	$(MAKE) package

tools: $(TOOLBUILDROOT)/Makefile
	mkdir -p $(BINROOT)/bin/
	@echo Building $(PACKAGE)
	cd $(TOOLBUILDROOT) && \
	    TMPDIR=$$PWD $(MAKE) CWARNINGFLAGS="-Wall -g -fno-omit-frame-pointer" \
	         acpibin acpiexamples acpihelp acpinames acpixtract iasl
ifeq ($(BUILD_OS), win32)
	@cd $(TOOLBUILDROOT)/generate/unix/bin && \
	for app in *; do \
	    cp -Rpv $(TOOLBUILDROOT)/generate/unix/bin/$$app \
	        $(TOOLBINROOT)/bin/$$app.exe ;\
	done
else
	cp -Rp $(TOOLBUILDROOT)/generate/unix/bin $(TOOLBINROOT)
endif

clean:
	rm -rf $(BUILDROOT)
	rm -rf $(TOOLBUILDROOT)
	rm -rf $(PATCHED_SOURCE_DIRECTORY)
	rm -rf $(ORIGINAL_SOURCE_DIRECTORY)

package:
	sh ./package.sh "$(BINROOT)" "$(PKGROOT)"

##
## This target recreates the diff file from the patched source directory.
##

recreate-diff: $(ORIGINAL_SOURCE_DIRECTORY)
	cd $(PATCHED_SOURCE_DIRECTORY) && diff -Nru3 -x .svn ../$(PACKAGE).orig . > $(DIFF_FILE) || test $$? = "1"

##
## Unpack the source to PACKAGE.orig.
##

$(ORIGINAL_SOURCE_DIRECTORY): $(SOURCE_TARBALL)
	rm -rf $(ORIGINAL_SOURCE_DIRECTORY)
	tar -xzf $^ -C $(OBJROOT)
	mv $(OBJROOT)/$(PACKAGE) $@

$(BUILDROOT)/Makefile: | $(BINROOT) $(PATCHED_SOURCE_DIRECTORY)
	@echo Copying source
	rm -rf "$(BUILDROOT)"
	cp -Rp "$(PATCHED_SOURCE_DIRECTORY)" "$(BUILDROOT)"

$(TOOLBUILDROOT)/Makefile: | $(BINROOT) $(PATCHED_SOURCE_DIRECTORY)
	@echo Copying source
	rm -rf "$(TOOLBUILDROOT)"
	cp -Rp "$(PATCHED_SOURCE_DIRECTORY)" "$(TOOLBUILDROOT)"

##
## Unpack the source to PACKAGE.src, then apply the patch.
##

$(PATCHED_SOURCE_DIRECTORY): $(SOURCE_TARBALL) | $(OBJROOT)
	rm -rf $(PATCHED_SOURCE_DIRECTORY)
	tar -xzf $(SOURCE_TARBALL) -C $(OBJROOT)
	mv $(OBJROOT)/$(PACKAGE) $@
	cd $(PATCHED_SOURCE_DIRECTORY) && $(PATCH) -p0 -i $(DIFF_FILE)

$(BINROOT):
	mkdir -p $@

