##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package_tools.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script tars up the x86 and ARMv7 tools. It assumes the tools have
##     already been built.
##
## Author:
##
##     Evan Green 25-Oct-2016
##
## Environment:
##
##     Build with POSIX tools.
##
## Adopted under Peresvet OS by:
##
##     Andrei Zaikin 06-Jul-2017
##

set -e

WORKING="$PWD/pkgtools$$"
os=`sh $SRCROOT/third-party/build/guessos.sh`

mkdir -p "$WORKING/src"
for d in x86dbg armv7dbg; do
    if [ -d "$SRCROOT/$d/tools" ]; then
        echo "Copying $d"
        mkdir "$WORKING/src/$d"
        cp -Rp "$SRCROOT/$d/tools" "$WORKING/src/$d"
    fi
done

OLDPWD=`pwd`
cd $WORKING
if [ "$os" = "win32" ]; then
    7za a -tzip -mmt -mx9 -mtc peresvet-tools-$os.zip ./src
    mv ./peresvet-tools-$os.zip $OLDPWD

else
    tar -czf ./peresvet-tools-$os.tar.gz ./src
    mv ./peresvet-tools-$os.tar.gz $OLDPWD
fi

cd "$OLDPWD"
rm -rf "$WORKING"

