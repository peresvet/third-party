##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the Berkeley yacc binaries.
##
## Author:
##
##     Evan Green 11-Feb-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr/bin"
cp -pv "$BUILD_DIRECTORY/usr/bin/yacc" "$PACKAGE_DIRECTORY/usr/bin/yacc"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: byacc
Priority: optional
Version: 20141128
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://invisible-island.net/datafiles/release/byacc.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Berkeley YACC
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

