################################################################################
#
#   Copyright (c) 2013 Minoca Corp. All Rights Reserved.
#
#   Module Name:
#
#       common.mk
#
#   Abstract:
#
#       This makefile contains common definitions for all packages.
#
#   Author:
#
#       Evan Green 23-Oct-2013
#
#   Environment:
#
#       Build
#
# Adopted under Peresvet OS by:
#
#     Andrei Zaikin 06-Jul-2017
#
################################################################################

##
## Check for the necessary environment variables.
##

ifndef SRCROOT
$(error Error: Environment not set up: SRCROOT environment variable missing)
endif

ifndef ARCH
$(error Error: Environment not set up: ARCH environment variable missing)
endif

ifndef DEBUG
$(error Error: Environment not set up: DEBUG environment variable missing)
endif

##
## Define build locations. Remove the drive letter, making something like
## c:/mydir/foo into /mydir/foo. There are too many places where the scripts
## and make get fouled up by colons.
##

ROOT := $(subst \,/,$(SRCROOT))
ROOT := $(shell echo $(ROOT) | sed 's/.*:\(.*\)/\1/')
OUTROOT := $(ROOT)/$(ARCH)$(VARIANT)$(DEBUG)
BINROOT := $(OUTROOT)/bin
TOOLBINROOT := $(OUTROOT)/tools
OBJROOT := $(OUTROOT)/obj/third-party

export MAKE := $(MAKE)
export PKGOUT := $(BINROOT)/packages
export DEPENDROOT := $(BINROOT)/dep
export SHELL
export TOOLBINROOT

##
## Figure out which OS the build machine is running.
##

export BUILD_OS := $(shell sh $(ROOT)/third-party/build/guessos.sh)

##
## The Quark runs i586, the rest of the world runs i686.
##

ifeq ($(ARCH)$(VARIANT), x86)
export TARGET=i686-pc-peresvet
export PACKAGE_ARCH=peresvet-i686
endif

ifeq ($(ARCH)$(VARIANT), x86q)
export TARGET=i586-pc-peresvet
export PACKAGE_ARCH=peresvet-i586
endif

ifeq ($(ARCH)$(VARIANT), x64)
export TARGET=x86_64-pc-peresvet
export PACKAGE_ARCH=peresvet-x86_64
endif

ifeq ($(ARCH)$(VARIANT), armv7)
export TARGET=arm-none-peresvet
export PACKAGE_ARCH=peresvet-armv7
endif

ifeq ($(ARCH)$(VARIANT), armv6)
export TARGET=arm-none-peresvet
export PACKAGE_ARCH=peresvet-armv6
endif

##
## Define some tool locations.
##

ifeq ($(BUILD_OS), win32)
PATCH := patsch
else
PATCH := patch
endif

CC := gcc
RANLIB := ranlib

##
## Define (the first) make targets.
##

all: | $(BINROOT)

$(BINROOT) $(TOOLBINROOT) $(OBJROOT):
	mkdir -p $@

##
## Don't let make use any builtin rules.
##

.SUFFIXES:

