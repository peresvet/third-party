##
## Copyright (c) 2014 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build_common.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script performs actions common to all build.sh scripts. It is
##     meant to be sourced directly into each build script.
##
## Author:
##
##     Evan Green 4-Dec-2014
##
## Environment:
##
##     Build with POSIX tools.
##
## Adopted under Peresvet OS by:
##
##     Andrei Zaikin 06-Jul-2017
##

set -e

##
## Grab the arguments.
##

SOURCE_DIRECTORY="$1"
BUILD_DIRECTORY="$2"
OUTPUT_DIRECTORY="$3"
BUILD_COMMAND="$4"

if test -z "$SOURCE_DIRECTORY"; then
  echo $0: Error: Source directory was not set.
  exit 1
fi

if test -z "$BUILD_DIRECTORY"; then
  echo $0: Error: Build directory was not set.
  exit 1
fi

if test -z "$OUTPUT_DIRECTORY"; then
  echo $0: Error: Output directory was not set.
  exit 1
fi

if test -z "$BUILD_COMMAND"; then
  echo $0: Error: Build command was not set.
  exit 1
fi

if test -z "$TARGET"; then
  echo $0: Error: TARGET is not set.
  exit 1
fi

if test -n "$NO_PARALLEL_MAKE"; then
    PARALLEL_MAKE=

else
    if test -z "$PARALLEL_MAKE"; then
        PARALLEL_MAKE=-j$((`nproc 2>/dev/null || echo 4` + 1))
    fi
fi

if test -z "$MAKE"; then
    MAKE=make
fi

##
## SHELL usually is determined internally by make. On Windows, this comes out
## to something like c:/src/tools/win32/build/swiss/sh.exe.
##

export CONFIG_SHELL="$SHELL"
export TMPDIR="$BUILD_DIRECTORY"

##
## Export some variables needed to build correctly on Windows.
##

if test "x$BUILD_OS" = "xwin32"; then
    export PATH_SEPARATOR=';'
    export ac_executable_extensions='.exe .bat'
    export TMPDIR=`echo $TEMP | sed -e 's_\\\\_/_g'`

else
    export PATH_SEPARATOR=':'
fi

##
## Set up pkg-config to point at $DEPENDROOT, convincingly.
##

if [ -x "$TOOLBINROOT/bin/pkg-config" ]; then
    export PKG_CONFIG="$TOOLBINROOT/bin/pkg-config"
else
    export PKG_CONFIG=`which pkg-config || which pkg-config.exe`
fi

export PKG_CONFIG_PATH="$DEPENDROOT/usr/lib/pkgconfig${PATH_SEPARATOR}\
$DEPENDROOT/usr/share/pkgconfig"

if test "x$BUILD_OS" = "xperesvet"; then
    BUILD_LINE="--build=$TARGET"
fi

if test -z "$CFLAGS"; then
  : # CFLAGS="-O2 -g -fno-omit-frame-pointer"
fi

##
## If building a native application for Quark, restrict to i586 and enable the
## LOCK prefix workaround.
##

if [ "$ARCH$VARIANT" = "x86q" ]; then
  if test "x$BUILD_COMMAND" != "xconfigure-tools"; then
    CFLAGS="$CFLAGS -Wa,-momit-lock-prefix=yes -march=i586"
  fi
fi

##
## If building a native application for ARMv6, restrict to armv6zk for the
## arm1176jzf-s. Also make sure that code generating binaries default to
## producing armv6zk.
##

if test "x$ARCH" = "xarmv6"; then
    if test "x$BUILD_COMMAND" != "xconfigure-tools"; then
      CFLAGS="$CFLAGS -march=armv6zk -marm -mfpu=vfp"
    fi
fi

##
## This function extracts a dependency package into a working directory. It
## takes a single argument, a string of <package>_<version>.
##

extract_dependency () {
    local package stamp extract_data
    if test -z "$DEPENDROOT"; then
        echo "$0: DEPENDROOT must be set."
        return 1
    fi

    if test -z "$PACKAGE_ARCH"; then
        echo "$0: PACKAGE_ARCH must be set."
        return 1
    fi

    if test -z "$PKGOUT"; then
        echo "$0: PKGOUT must be set."
        return 1
    fi

    package="$PKGOUT/${1}_${PACKAGE_ARCH}.ipk"
    if ! test -r "$package"; then
        package_all="$PKGOUT/${1}_all.ipk"
        if test -r "$package_all"; then
            package="$package_all"

        else
            echo "$0: Error: $package not found. You may need to go build it."
            return 2
        fi
    fi

    stamp="$DEPENDROOT/stamps/${1}.stamp"
    extract_data="$SRCROOT/third-party/build/opkg-utils/opkg-extract-data"
    if ! [ -r "$stamp" ] || [ "$package" -nt "$stamp" ]; then
        mkdir -p "$DEPENDROOT/stamps"
        sh "$extract_data" "$package" "$DEPENDROOT"
        touch "$stamp"
    fi

    return
}

