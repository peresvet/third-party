##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     x_common.sh
##
## Abstract:
##
##     This script performs actions common to all Xorg build components.
##
## Author:
##
##     Evan Green 21-Mar-2017
##
## Environment:
##
##     Build with POSIX tools.
##
## Adopted under Peresvet OS by:
##
##     Andrei Zaikin 06-Jul-2017
##

export XORG_PREFIX=/usr

XORG_CONFIG="$BUILD_LINE \
 --host=$TARGET \
 --prefix=$XORG_PREFIX \
 --sysconfdir=/etc \
 --localstatedir=/var \
 --disable-static"

export LIBRARY_PATH="$DEPENDROOT/usr/lib"
export C_INCLUDE_PATH="$DEPENDROOT/usr/include"
export CPLUS_INCLUDE_PATH="$DEPENDROOT/usr/include"

export CFLAGS="$CFLAGS -I$C_INCLUDE_PATH"
export CXXFLAGS="$CXXFLAGS -I$C_INCLUDE_PATH"
export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link,$DEPENDROOT/usr/lib"

export ACLOCAL="aclocal -I $DEPENDROOT/usr/share/aclocal"

if [ "$BUILD_OS" = "win32" ]; then
    export xorg_cv_malloc0_returns_null=no
fi

