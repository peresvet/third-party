##
## Copyright (c) 2014 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     copyheaders.sh <source_root> <install_directory>
##
## Abstract:
##
##     This script copies the C Library headers into place.
##
## Author:
##
##     Evan Green 2-Jul-2014
##
## Environment:
##
##     Build
##
## Adopted under Peresvet OS by:
##
##     Andrei Zaikin 06-Jul-2017
##

set -e

SOURCE_ROOT="$1"
INSTALL_DIRECTORY="$2"
mkdir -p "$INSTALL_DIRECTORY"

INCLUDE_SOURCE=${SOURCE_ROOT}/os/apps/libc/include
if ! test -f ${INCLUDE_SOURCE}/stdio.h; then
  echo "$0: Error: Headers don't exist at $INCLUDE_SOURCE."
  exit 2
fi

if test -z "$TARGET"; then
  echo $0: Error: TARGET is not set.
  exit 1
fi

echo TARGET=$TARGET

INCLUDE_DESTINATION=${INSTALL_DIRECTORY}/include
mkdir -p ${INCLUDE_DESTINATION}
ORIGINAL_DIRECTORY=`pwd`
cd ${INCLUDE_SOURCE}
FILES=`find . -name '*.h'`
for FILE in $FILES; do
  mkdir -p $INCLUDE_DESTINATION/`dirname $FILE`
  if ! [ -r "$INCLUDE_DESTINATION/$FILE" ] || \
     [ "$FILE" -nt "$INCLUDE_DESTINATION/$FILE" ]; then

      cp -fpv $FILE $INCLUDE_DESTINATION/$FILE
  fi
done

cp -Rpv "${SOURCE_ROOT}/os/include/peresvet/" "${INCLUDE_DESTINATION}"
cd $ORIGINAL_DIRECTORY
echo "Done Copying Headers."
exit 0

