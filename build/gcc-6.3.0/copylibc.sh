##
## Copyright (c) 2013 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     copylibc.sh <source_root> <install_directory> [<is_tool>]
##
## Abstract:
##
##     This script copies the built C library into place.
##
## Author:
##
##     Evan Green 24-Oct-2013
##
## Environment:
##
##     Build
##
## Adopted under Peresvet OS by:
##
##     Andrei Zaikin 06-Jul-2017
##

set -e

SOURCE_ROOT="$1"
INSTALL_DIRECTORY="$2"
IS_TOOL="$3"

GCC_VERSION=6.3.0

mkdir -p "$INSTALL_DIRECTORY"

case $ARCH in
  x86 | x64)
    LIB_CRT_ARCH=$ARCH
    ;;

  armv[67])
    LIB_CRT_ARCH=armv7
    ;;

  *)
    echo "Invalid architecture $ARCH."
    exit 3
    ;;

esac

if test -z "$TARGET"; then
  echo $0: Error: TARGET is not set.
  exit 1
fi

echo TARGET=$TARGET
LIB_SOURCE=${SOURCE_ROOT}/${ARCH}${VARIANT}${DEBUG}/obj/os/apps/libc
BINROOT=${SOURCE_ROOT}/${ARCH}${VARIANT}${DEBUG}/bin
LIB_CRT_PATH=${LIB_SOURCE}/static/${LIB_CRT_ARCH}/crt0.o

if ! test -f ${LIB_CRT_PATH}; then
  echo "$0: Error: crt0.o doesn't exist at ${LIB_CRT_PATH}."
  exit 3
fi

LIB_DESTINATION=${INSTALL_DIRECTORY}/lib

mkdir -p ${LIB_DESTINATION}
cp -fpv ${LIB_CRT_PATH} ${LIB_DESTINATION}
cp -fpv ${LIB_DESTINATION}/crt0.o ${LIB_DESTINATION}/Scrt0.o
cp -fpv ${LIB_DESTINATION}/crt0.o ${LIB_DESTINATION}/gcrt0.o

##
## For the purposes of building, copy the dynamic libraries into the
## destination.
##

LIBS="libc_nonshared.a
libpthread_nonshared.a
libperesvetos.so.1
libc.so.1
libcrypt.so.1"

for file in $LIBS; do
    cp -fpv "$BINROOT/$file" "$LIB_DESTINATION/"
done

##
## Copy the dynamic library linker scripts.
##

for file in ./libs/*; do
    cp -fpv "$file" "$LIB_DESTINATION/"
done

echo "Done Copying C Library."
exit 0

