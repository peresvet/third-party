##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the GCC compiler package.
##
## Author:
##
##     Evan Green 18-Jan-2017
##
## Environment:
##
##     Build
##
## Adopted under Peresvet OS by:
##
##     Andrei Zaikin 06-Jul-2017
##

. ../build_common.sh

GCC_VERSION=6.3.0

LIBGMP=libgmp_6.1.2
LIBMPFR=libmpfr_3.1.5
LIBMPC=libmpc_1.0.3

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure-tools)
    export AR='ar'
    if test "x$ARCH" = "xarmv6"; then
      ARCH_CONFIG_ARGS="--with-arch=armv6zk --with-fpu=vfp --disable-libatomic"
    fi

    case "$ARCH" in
    armv7) WITH_MODE="--with-mode=thumb" ;;
    esac

    export CFLAGS="$CFLAGS -I$OUTPUT_DIRECTORY/build/include"
    export LDFLAGS="$LDFLAGS -L$OUTPUT_DIRECTORY/build/lib"
    CXXFLAGS="$CFLAGS"
    export CXXFLAGS_FOR_TARGET=" $CXXFLAGS"
    [ "$BUILD_OS" = "macos" ] && \
        CXXFLAGS="$CXXFLAGS -fbracket-depth=512"

    sh ${SOURCE_DIRECTORY}/configure --prefix="$OUTPUT_DIRECTORY" \
                                     --with-sysroot="$OUTPUT_DIRECTORY" \
                                     --with-build-sysroot="$OUTPUT_DIRECTORY" \
                                     --enable-version-specific-runtime-libs \
                                     --target=$TARGET \
                                     --enable-languages=c,c++ \
                                     --disable-nls \
                                     --enable-thread=posix \
                                     --disable-win32-registry \
                                     --enable-initfini-array \
                                     --disable-bootstrap \
                                     --with-gnu-ld \
                                     $WITH_MODE \
                                     $ARCH_CONFIG_ARGS \
                                     CFLAGS="$CFLAGS" \
                                     CXXFLAGS="$CXXFLAGS" \
                                     LDFLAGS="$LDFLAGS" \

    ;;

  ##
  ## Configure for building a native Minoca compiler (host and target are the
  ## same).
  ##

  configure)
    export CC="$TARGET-gcc"
    if test "x$ARCH" = "xarmv6"; then
      ARCH_CONFIG_ARGS="--with-arch=armv6zk --with-fpu=vfp --disable-libatomic"
    fi

    case "$ARCH" in
    armv7) WITH_MODE="--with-mode=thumb" ;;
    esac

    ##
    ## GCC fails to cross compile gengtype-lex.c, as it seems to include both
    ## auto-host.h and auto-build-h, which redefine many of the same macros.
    ##

    [ "$BUILD_OS" = "peresvet" ] || DISABLE_PLUGIN="--disable-plugin"

    extract_dependency "$LIBGMP"
    extract_dependency "$LIBMPFR"
    extract_dependency "$LIBMPC"

    export CFLAGS="-I$DEPENDROOT/usr/include"
    export CXXFLAGS="$CFLAGS"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    sh ${SOURCE_DIRECTORY}/configure --prefix="/usr" \
                                     --with-sysroot="/" \
                                     --with-build-sysroot="$OUTPUT_DIRECTORY/" \
                                     --enable-version-specific-runtime-libs \
                                     $BUILD_LINE \
                                     --host=$TARGET \
                                     --target=$TARGET \
                                     --enable-languages=c,c++ \
                                     --disable-nls \
                                     --enable-threads=posix \
                                     --disable-bootstrap \
                                     --enable-multilib \
                                     --with-gnu-ld \
                                     $DISABLE_PLUGIN \
                                     $WITH_MODE \
                                     $ARCH_CONFIG_ARGS \
                                     CPPFLAGS="$CPPFLAGS" \
                                     CFLAGS="$CFLAGS" \
                                     CXXFLAGS="$CXXFLAGS" \
                                     LDFLAGS="$LDFLAGS" \

    ;;

  build-all-gcc)
    $MAKE $PARALLEL_MAKE all-gcc
    ;;

  build-tools)
    export TMPDIR=.
    $MAKE $PARALLEL_MAKE
    $MAKE install
    GCCLIB="gcc/$TARGET/$GCC_VERSION"
    cd "$OUTPUT_DIRECTORY/lib"
    if [ -f "$GCCLIB/libgcc_s.so.1" ]; then
        ln -sf "$GCCLIB/libgcc_s.so.1" libgcc_s.so.1
        ln -sf libgcc_s.so.1 libgcc_s.so
    fi

    ;;

  build)
    export TMPDIR=.
    $MAKE $PARALLEL_MAKE
    $MAKE install DESTDIR="$OUTPUT_DIRECTORY/"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

