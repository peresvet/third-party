################################################################################
#
#   Copyright (c) 2013 Minoca Corp. All Rights Reserved.
#
#   Module Name:
#
#       Third Party
#
#   Abstract:
#
#       This makefile is responsible for building third party packages.
#
#   Author:
#
#       Evan Green 23-Oct-2013
#
#   Environment:
#
#       Build
#
################################################################################

include build/common.mk

##
## Define the minimal set of tools needed to build the OS repository.
##

OS_TOOLS := awk                  \
            binutils-2.27        \
            gmp-6.1.2            \
            mpfr-3.1.5           \
            mpc-1.0.3            \
            gcc-6.3.0            \
            byacc-20141128       \
            acpica-unix-20140424 \
            ninja-1.7.1          \

ifneq ($(BUILD_OS),win32)

OS_TOOLS += flex-2.5.39          \

endif

##
## Define the set of tools needed to build the entire third-party repository.
##

ALL_TOOLS := $(OS_TOOLS)          \
             libiconv-1.14        \
             ncurses-5.9          \
             dmake-DMAKE_4_12_2_2 \
             openssl-1.0.2h       \
             m4-1.4.17            \
             autoconf-2.69        \
             automake-1.15        \
             libtool-2.4.6        \
             cmake-3.5.2          \
             pkg-config-0.29.1    \
             yasm-1.3.0           \
             expat-2.1.0          \

##
## Define the set of packages that are compiled as tools for the build machine
## but may not be compiled for Minoca. The reason for this is usually that the
## tool cannot be cross compiled.
##

TOOLS_ONLY := perl-5.24.1        \

##
## Define all the packages, omitting any packages listed in the tools section
## above.
##

APPS := zlib-1.2.11              \
        make-3.82                \
        tar-1.27.1               \
        gzip-1.6                 \
        patch-2.7                \
        readline-6.3             \
        sqlite-autoconf-3080500  \
        nano-2.2.6               \
        vttest-20140305          \
        xz-5.2.1                 \
        bzip2-1.0.6              \
        pcre-8.39                \
        wget-1.15                \
        opkg-0.2.4               \
        shadow-4.2.1             \
        apr-1.5.1                \
        apr-util-1.5.4           \
        subversion-1.8.11        \
        bash-4.3.30              \
        dash-0.5.8               \
        mbedtls-1.3.10           \
        libssh2-1.5.0            \
        curl-7.41.0              \
        git-2.3.5                \
        vim-7.4                  \
        openssh-6.7p1            \
        ca-certificates          \
        bison-3.0.2              \
        tcl8.6.5                 \
        pcre2-10.21              \
        libxml2-2.9.4            \
        httpd-2.4.20             \
        lua-5.3.3                \
        less-481                 \
        libevent-2.0.22-stable   \
        tmux-2.2                 \
        libunistring-0.9.6       \
        libffi-3.2.1             \
        libatomic_ops-7.4.4      \
        gc-7.4.4                 \
        dropbear-2016.74         \
        rsync-3.1.2              \
        postgresql-9.5.4         \
        gettext-0.19.8.1         \
        glib-2.49.6              \
        libcroco-0.6.11          \
        gdbm-1.12                \
        zsh-5.2                  \
        libgpg-error-1.24        \
        libassuan-2.4.3          \
        pinentry-0.9.7           \
        libgcrypt-1.7.3          \
        libksba-1.3.5            \
        npth-1.2                 \
        nettle-3.2               \
        libtasn1-4.9             \
        p11-kit-0.23.2           \
        libidn-1.33              \
        unbound-1.5.9            \
        gnutls-3.5.3             \
        openldap-2.4.44          \
        gnupg-2.1.15             \
        gpgme-1.6.0              \
        pth-2.0.7                \
        irssi-1.0.0              \
        cpio-2.12                \
        mc-4.8.18                \
        nasm-2.12.02             \
        libx264-20170102-2245    \
        libjpeg-turbo-1.5.1      \
        libexif-0.6.21           \
        libid3tag-0.15.1b        \
        libogg-1.3.2             \
        flac-1.3.2               \
        libvorbis-1.3.5          \
        ffmpeg-3.2.1             \
        groff-1.22.3             \
        libpng-1.6.28            \
        freetype-prebuild        \
        harfbuzz-1.4.4           \
        freetype-2.7.1           \
        minidlna-1.1.5           \
        fontconfig-2.12.1        \
        pixman-0.34.0            \
        tiff-4.0.7               \
        hicolor-icon-theme-0.15  \
        gperftools-2.5           \

##
## Define the minimal set of packages needed to install opkg and a basic SSH
## connection. Note that these are packages, rather than directories, because
## they get fed into the install package script.
##

MINIMAL := libgcc-6.3.0             \
           libz-1.2.11              \
           libiconv-1.14            \
           ca-certificates-20161215 \
           libopenssl-1.0.2h        \
           gzip-1.6                 \
           libncurses-5.9           \
           libreadline-6.3          \
           nano-2.2.6               \
           libpcre-8.39             \
           wget-1.15                \
           opkg-0.2.4               \
           tar-1.27.1               \
           dropbear-2016.74         \

##
## Define the list of packages needed for the automation. These packages can
## all be cross compiled.
##

AUTOMATION := opkg-0.2.4              \
              awk                     \
              binutils-2.27           \
              gcc-6.3.0               \
              byacc-20141128          \
              flex-2.5.39             \
              gzip-1.6                \
              m4-1.4.17               \
              make-3.82               \
              nano-2.2.6              \
              zlib-1.2.11             \
              openssh-6.7p1           \
              patch-2.7               \
              expat-2.1.0             \
              tar-1.27.1              \
              pcre-8.39               \
              wget-1.15               \
              acpica-unix-20140424    \
              bzip2-1.0.6             \
              sqlite-autoconf-3080500 \
              libiconv-1.14           \
              ncurses-5.9             \
              readline-6.3            \
              openssl-1.0.2h          \
              ca-certificates         \
              pkg-config-0.29.1       \
              yasm-1.3.0

X_HEADERS := util-macros-1.19.1       \
             bigreqsproto-1.1.2       \
             compositeproto-0.4.2     \
             damageproto-1.2.1        \
             dmxproto-2.3.1           \
             dri2proto-2.8            \
             dri3proto-1.0            \
             fixesproto-5.0           \
             fontsproto-2.1.3         \
             glproto-1.4.17           \
             inputproto-2.3.2         \
             kbproto-1.0.7            \
             presentproto-1.1         \
             randrproto-1.5.0         \
             recordproto-1.14.2       \
             renderproto-0.11.1       \
             resourceproto-1.2.0      \
             scrnsaverproto-1.2.2     \
             videoproto-2.3.3         \
             xcmiscproto-1.2.2        \
             xextproto-7.3.0          \
             xf86bigfontproto-1.2.0   \
             xf86dgaproto-2.1         \
             xf86driproto-2.1.1       \
             xf86vidmodeproto-2.3.1   \
             xineramaproto-1.2.1      \
             xproto-7.0.31

X_PRELIBS := libXau-1.0.8             \
             libXdmcp-1.1.2           \
             xcb-proto-1.12           \
             libxcb-1.12              \

X_LIBS := xtrans-1.3.5                \
          libX11-1.6.5                \
          libXext-1.3.3               \
          libFS-1.0.7                 \
          libICE-1.0.9                \
          libSM-1.2.2                 \
          libXScrnSaver-1.2.2         \
          libXt-1.1.5                 \
          libXmu-1.1.2                \
          libXpm-3.5.12               \
          libXaw-1.0.13               \
          libXfixes-5.0.3             \
          libXcomposite-0.4.4         \
          libXrender-0.9.10           \
          libXcursor-1.1.14           \
          libXdamage-1.1.4            \
          libfontenc-1.1.3            \
          libXfont2-2.0.1             \
          libXft-2.3.2                \
          libXi-1.7.9                 \
          libXinerama-1.1.3           \
          libXrandr-1.5.1             \
          libXres-1.0.7               \
          libXtst-1.2.3               \
          libXv-1.0.11                \
          libXvMC-1.0.10              \
          libXxf86dga-1.1.4           \
          libXxf86vm-1.1.4            \
          libdmx-1.1.3                \
          libxkbfile-1.0.9            \
          libxshmfence-1.2            \
          xcb-util-0.4.0              \
          xcb-util-image-0.4.0        \
          xcb-util-keysyms-0.4.0      \
          xcb-util-renderutil-0.3.9   \
          xcb-util-wm-0.4.1           \
          mesa-17.0.0                 \
          glu-9.0.0                   \
          glew-2.0.0                  \
          xbitmaps-1.1.1              \

X_UTILITIES := iceauth-1.0.7          \
               luit-1.1.1             \
               mkfontdir-1.0.7        \
               mkfontscale-1.1.2      \
               sessreg-1.1.1          \
               setxkbmap-1.3.1        \
               smproxy-1.0.6          \
               x11perf-1.6.0          \
               xauth-1.0.10           \
               xbacklight-1.2.1       \
               xcmsdb-1.0.5           \
               xcursorgen-1.0.6       \
               xdpyinfo-1.3.2         \
               xdriinfo-1.0.5         \
               xev-1.2.2              \
               xgamma-1.0.6           \
               xhost-1.0.7            \
               xinput-1.6.2           \
               xkbcomp-1.3.1          \
               xkbevd-1.1.4           \
               xkbutils-1.0.4         \
               xkill-1.0.4            \
               xlsatoms-1.1.2         \
               xlsclients-1.1.3       \
               xmessage-1.0.4         \
               xmodmap-1.0.9          \
               xpr-1.0.4              \
               xprop-1.2.2            \
               xrandr-1.5.0           \
               xrdb-1.1.0             \
               xrefresh-1.0.5         \
               xset-1.2.3             \
               xsetroot-1.1.1         \
               xvinfo-1.1.3           \
               xwd-1.0.6              \
               xwininfo-1.1.3         \
               xwud-1.0.4             \
               font-util-1.3.1        \
               font-alias-1.0.3       \

X_FONTS := xcursor-themes-1.0.4          \
           encodings-1.0.4               \
           font-adobe-utopia-type1-1.0.4 \
           font-bh-ttf-1.0.3             \
           font-bh-type1-1.0.3           \
           font-ibm-type1-1.0.3          \
           font-misc-ethiopic-1.0.3      \
           font-xfree86-type1-1.0.4      \
           intltool-0.50.2               \
           xkeyboard-config-2.20         \
           freeglut-3.0.0                \
           mesa-demos-8.3.0              \

X := xorg-server-1.19.3 \

X_APPS := xf86-video-fbdev-0.4.4 \
          xf86-input-minoca      \
          twm-1.0.9              \
          xterm-327              \
          xclock-1.0.7           \
          xinit-1.3.4            \

ifeq ($(BUILD_OS),minoca)

X_APPS += atk-2.24               \
          cairo-1.14.8           \
          pango-1.40.5           \
          gdk-pixbuf-2.36.6      \
          gtk+-2.24.31           \
          SDL-1.2.15             \
          SDL2-2.0.5             \
          fceux-2.2.3            \

endif

X_PACKAGES := $(X_HEADERS) $(X_PRELIBS) $(X_LIBS) $(X_UTILITIES) $(X) $(X_APPS)

##
## Define the additional list of packages needed by the automation that cannot
## be cross compiled.
##

AUTOMATION_NATIVE_ONLY := perl-5.24.1   \
                          Python-2.7.11 \

##
## Some packages can only be compiled natively (they cannot be cross compiled).
##

ifeq ($(BUILD_OS),minoca)

APPS += perl-5.24.1             \
        Python-2.7.11           \
        Python-3.5.2            \
        expect5.45              \
        dejagnu-1.6             \
        setuptools-23.0.0       \
        pip-8.1.2               \
        nginx-1.10.1            \
        emacs-24.5              \
        screen-4.4.0            \
        guile-2.0.12            \
        boost_1_59_0            \
        mysql-5.7.13            \
        autogen-5.18.10         \
        node-v4.5.0             \
        ruby-2.3.1              \
        php-7.0.9               \
        cloud-init-0.7.6        \
        DirectFB-DIRECTFB_1_7_7 \
        scons-src-2.5.1         \
        file-5.30               \
        samba-4.6.4             \

X_PACKAGES += $(X_FONTS)

AUTOMATION += $(AUTOMATION_NATIVE_ONLY)

endif

ifeq ($(BUILD_OS),win32)

APPS += flex-2.5.39             \

endif


PACKAGES := $(ALL_TOOLS) $(APPS)

PACKAGES_TOOLS := %-tools
PACKAGES_CLEAN := %-clean
BUILD_STAMP := $(OBJROOT)/%.build/build.stamp
TOOL_STAMP := $(OBJROOT)/%.tool/build.stamp

##
## This function definition, when called with 2 arguments, evaluates to the
## following (paraphrased):
## <1>.stamp: <2>.stamp <3>.stamp ...
## where <2>, <3>, <4>, etc, consist of argument 2 split up on spaces.
##

dep = $(1:%=$(BUILD_STAMP)): $(foreach pkg,$(2),$(pkg:%=$(BUILD_STAMP)))
tool_dep = $(1:%=$(TOOL_STAMP)): $(foreach pkg,$(2),$(pkg:%=$(TOOL_STAMP)))

all: $(PACKAGES)
x: $(X_PACKAGES)
all-tools: all-tools-quiet
all-tools-quiet: $(ALL_TOOLS:%=$(PACKAGES_TOOLS)) $(TOOLS_ONLY:%=$(PACKAGES_TOOLS))
tools: $(OS_TOOLS:%=$(PACKAGES_TOOLS))
install-minimal: $(MINIMAL)
clean: $(PACKAGES:%=$(PACKAGES_CLEAN))
automation: $(AUTOMATION)
automation-native-only: $(AUTOMATION_NATIVE_ONLY)

all all-tools tools clean automation automation-native-only x:
	@echo Completed building $@

install-minimal: $(MINIMAL)
	@set -e; \
	 for pkg in $^; do \
	     echo "Installing $$pkg" ; \
	     sh build/install_package.sh $$pkg ; \
	 done
	@sh build/minimal_config.sh

package-tools: tools
	@sh build/package_tools.sh
	@echo Completed packaging tools

.PHONY: all x all-tools all-tools-quiet tool $(PACKAGES) libgcc_6.3.0
.PHONY: $(ALL_TOOLS:%=$(PACKAGES_TOOLS)) $(TOOLS_ONLY:%=$(PACKAGES_TOOLS))
.PHONY: clean $(PACKAGES:%=$(PACKAGES_CLEAN)) package-tools install-minimal
.PHONY: automation automation-native-only

##
## Relabel some packages to their respective directories.
##

libgcc-6.3.0: gcc-6.3.0
libz-1.2.11: zlib-1.2.11
ca-certificates-20161215: ca-certificates
libopenssl-1.0.2h: openssl-1.0.2h
libncurses-5.9: ncurses-5.9
libreadline-6.3: readline-6.3
libpcre-8.39: pcre-8.39

##
## For packages, just recurse into that directory. Each packages depends on its
## corresponding stamp file, and depends on the tools being built.
## These static pattern rules in the form <1>: <2>: <3> say "for each target
## in <1>, apply pattern <2> to get prerequisites list <3> for that target."
##

$(PACKAGES) $(X_PACKAGES): %: $(BUILD_STAMP) all-tools-quiet

##
## The packages-tools work similarly, except they depend on the tool stamp
## file. To build ALL_TOOLS and TOOLS_ONLY (with -tools appended to
## differentiate from PACKAGES), get the stamp file for that tool.
##

$(ALL_TOOLS:%=$(PACKAGES_TOOLS)) $(TOOLS_ONLY:%=$(PACKAGES_TOOLS)): $(PACKAGES_TOOLS): $(TOOL_STAMP)

##
## To build the stamps, just touch them. Each stamp is dependent on its package.
##

$(PACKAGES:%=$(BUILD_STAMP)) $(X_PACKAGES:%=$(BUILD_STAMP)):
	@echo Making $@
	@$(MAKE) -C build/$(@:$(BUILD_STAMP)=%)
	@echo Done making $@
	@mkdir -p `dirname $@`
	@date > $@

$(ALL_TOOLS:%=$(TOOL_STAMP)) $(TOOLS_ONLY:%=$(TOOL_STAMP)):
	@echo Making $@
	@$(MAKE) -C build/$(@:$(TOOL_STAMP)=%) tools
	@echo Done making $@
	@mkdir -p `dirname $@`
	@date > $@

##
## To clean a package, run make clean in it.
##

$(PACKAGES:%=$(PACKAGES_CLEAN)):
	@$(MAKE) -C build/$(@:$(PACKAGES_CLEAN)=%) clean

##
## Define dependencies.
##

$(call dep,binutils-2.27,awk)
$(call tool_dep,binutils-2.27,awk)
$(call dep,mpfr-3.1.5,gmp-6.1.2)
$(call tool_dep,mpfr-3.1.5,gmp-6.1.2)
$(call dep,mpc-1.0.3,mpfr-3.1.5)
$(call tool_dep,mpc-1.0.3,mpfr-3.1.5)
$(call dep,gcc-6.3.0,binutils-2.27 gmp-6.1.2 mpfr-3.1.5 mpc-1.0.3 awk)
$(call tool_dep,gcc-6.3.0,binutils-2.27 gmp-6.1.2 mpfr-3.1.5 mpc-1.0.3 awk)
$(call tool_dep,openssl-1.0.2h,perl-5.24.1)
$(call dep,acpica-unix-20140424,byacc-20141128)
$(call tool_dep,acpica-unix-20140424,byacc-20141128)
$(call dep,cmake-3.5.2,ncurses-5.9)
$(call tool_dep,cmake-3.5.2,ncurses-5.9)
$(call dep,pkg-config-0.29.1,libiconv-1.14)
$(call tool_dep,pkg-config-0.29.1,libiconv-1.14)

ifeq ($(BUILD_OS),win32)

$(call tool_dep,perl-5.24.1,dmake-DMAKE_4_12_2_2)

else

$(call tool_dep,acpica-unix-20140424,flex-2.5.39)

endif

$(call dep,Python-2.7.11,sqlite-autoconf-3080500 ncurses-5.9 \
    zlib-1.2.11 openssl-1.0.2h bzip2-1.0.6)

$(call dep,Python-3.5.2,sqlite-autoconf-3080500 ncurses-5.9 \
    zlib-1.2.11 openssl-1.0.2h bzip2-1.0.6)

$(call dep,readline-6.3,ncurses-5.9)
$(call dep,sqlite-autoconf-3080500,readline-6.3)
$(call dep,nano-2.2.6,readline-6.3)
$(call dep,openssh-6.7p1,openssl-1.0.2h)
$(call dep,apr-util-1.5.4,apr-1.5.1 sqlite-autoconf-3080500,openssl-1.0.2h)
$(call dep,subversion-1.8.11,apr-util-1.5.4)
$(call dep,curl-7.41.0,libssh2-1.5.0)
$(call dep,git-2.3.5,curl-7.41.0 libiconv-1.14 zlib-1.2.11 \
    subversion-1.8.11)

$(call dep,libssh2-1.5.0,openssl-1.0.2h)
$(call dep,curl-7.41.0,libssh2-1.5.0)
$(call dep,automake-1.15,autoconf-2.69)
$(call dep,libtool-2.4.6,autoconf-2.69)
$(call dep,tcl8.6.5,zlib-1.2.11)
$(call dep,libxml2-2.9.4,zlib-1.2.11 libiconv-1.14 readline-6.3)
$(call dep,httpd-2.4.20,apr-util-1.5.4 pcre-8.39 openssl-1.0.2h libxml2-2.9.4)
$(call dep,lua-5.3.3,readline-6.3)
$(call dep,less-481,ncurses-5.9)
$(call dep,emacs-24.5,ncurses-5.9 libxml2-2.9.4 libiconv-1.14)
$(call dep,libevent-2.0.22-stable,openssl-1.0.2h)
$(call dep,tmux-2.2,libevent-2.0.22-stable ncurses-5.9)
$(call dep,libunistring-0.9.6,libiconv-1.14)
$(call dep,gc-7.4.4,libatomic_ops-7.4.4)
$(call dep,screen-4.4.0,ncurses-5.9)
$(call dep,dropbear-2016.74,zlib-1.2.11)
$(call dep,rsync-3.1.2,zlib-1.2.11)
$(call dep,guile-2.0.12,libgc-7.4.4 libffi-3.2.1 libunistring-0.9.6 \
    libiconv-1.14 readline-6.3)

$(call dep,autogen-5.18.10,guile-2.0.12)
$(call dep,node-v4.5.0,openssl-1.0.2h zlib-1.2.11)
$(call dep,postgresql-9.5.4,readline-6.3)
$(call dep,php-7.0.9,libxml2-2.9.4 readline-6.3 gmp-6.1.2 bzip2-1.0.6)
$(call dep,gettext-0.19.8.1,libiconv-1.14 libxml2-2.9.4 \
    libunistring-0.9.6 ncurses-5.9)

$(call dep,glib-2.49.6,libiconv-1.14 pcre-8.39 libffi-3.2.1 gettext-0.19.8.1)
$(call dep,libcroco-0.6.11,glib-2.49.6 libxml2-2.9.4)
$(call dep,gdbm-1.12,libiconv-1.14 gettext-0.19.8.1)
$(call dep,zsh-5.2,ncurses-5.9 libiconv-1.14 pcre-8.39 gdbm-1.12)
$(call dep,libgpg-error-1.24,libiconv-1.14 gettext-0.19.8.1)
$(call dep,libassuan-2.4.3,libgpg-error-1.24)
$(call dep,pinentry-0.9.7,libassuan-2.4.3 libgpg-error-1.24 \
    libiconv-1.14 ncurses-5.9)

$(call dep,libgcrypt-1.7.3,libgpg-error-1.24)
$(call dep,libksba-1.3.5,libgpg-error-1.24)
$(call dep,nettle-3.2,gmp-6.1.2)
$(call dep,p11-kit-0.23.2,libffi-3.2.1 libtasn1-4.9 gettext-0.19.8.1)
$(call dep,libidn-1.33,libiconv-1.14 gettext-0.19.8.1)
$(call dep,unbound-1.5.9,libiconv-1.14 gettext-0.19.8.1 openssl-1.0.2h \
    libevent-2.0.22-stable expat-2.1.0)

$(call dep,gnutls-3.5.3,gmp-6.1.2 nettle-3.2 p11-kit-0.23.2 \
    libiconv-1.14 gettext-0.19.8.1 zlib-1.2.11 libtasn1-4.9 libidn-1.33 \
    unbound-1.5.9)

$(call dep,openldap-2.4.44,openssl-1.0.2h libtool-2.4.6)
$(call dep,gnupg-2.1.15,libgpg-error-1.24 libassuan-2.4.3 \
    libgcrypt-1.7.3 libksba-1.3.5 npth-1.2 ncurses-5.9 readline-6.3 \
    gettext-0.19.8.1 sqlite-autoconf-3080500 bzip2-1.0.6 openldap-2.4.44)

$(call dep,gpgme-1.6.0,libgpg-error-1.24 libassuan-2.4.3)
$(call dep,expect5.45,tcl8.6.5)
$(call dep,dejagnu-1.6,expect5.45)
$(call dep,setuptools-23.0.0,Python-2.7.11)
$(call dep,pip-8.1.2,setuptools-23.0.0)
$(call dep,nginx-1.10.1,openssl-1.0.2h pcre-8.39 zlib-1.2.11)
$(call dep,boost_1_59_0,Python-2.7.11 bzip2-1.0.6 zlib-1.2.11)
$(call dep,mysql-5.7.13,ncurses-5.9 cmake-3.5.2 boost_1_59_0)
$(call dep,irssi-1.0.0,glib-2.49.6 ncurses-5.9 openssl-1.0.2h)
$(call dep,cpio-2.12,libiconv-1.14 gettext-0.19.8.1)
$(call dep,mc-4.8.18,glib-2.49.6 ncurses-5.9 pcre-8.39)
$(call dep,cloud-init-0.7.6,Python-2.7.11 setuptools-23.0.0)
$(call dep,libid3tag-0.15.1b,zlib-1.2.11)
$(call dep,flac-1.3.2,libogg-1.3.2)
$(call dep,libvorbis-1.3.5,libogg-1.3.2)
$(call dep,ffmpeg-3.2.1,pkg-config-0.29.1 libx264-20170102-2245 libvorbis-1.3.5)
$(call dep,groff-1.22.3,libiconv-1.14)
$(call dep,DirectFB-DIRECTFB_1_7_7,freetype-2.7.1 libpng-1.6.28 \
    libjpeg-turbo-1.5.1)

$(call dep,libpng-1.6.28,zlib-1.2.11)
$(call dep,freetype-prebuild,libpng-1.6.28 bzip2-1.0.6 zlib-1.2.11)
$(call dep,harfbuzz-1.4.4,freetype-prebuild glib-2.49.6 gettext-0.19.8.1)
$(call dep,freetype-2.7.1,harfbuzz-1.4.4)
$(call dep,minidlna-1.1.5,ffmpeg-3.2.1 libjpeg-turbo-1.5.1 \
    sqlite-autoconf-3080500 libexif-0.6.21 libid3tag-0.15.1b flac-1.3.2)

$(call dep,scons-src-2.5.1,Python-2.7.11)
$(call dep,fontconfig-2.12.1,freetype-2.7.1 expat-2.1.0)
$(call dep,tiff-4.0.7,zlib-1.2.11)
$(call dep,samba-4.6.4,Python-2.7.11 gettext_0.19.8.1 openldap_2.4.44)
$(call dep,libXau-1.0.8,xproto-7.0.31 util-macros-1.19.1)
$(call dep,libXdmcp-1.1.2,xproto-7.0.31 util-macros-1.19.1)

ifeq ($(BUILD_OS),minoca)
$(call dep,xcb-proto-1.12,Python-2.7.11)
endif

$(call dep,libxcb-1.12,xcb-proto-1.12 libXau-1.0.8 xproto-7.0.31 \
    util-macros-1.19.1)

$(call dep,libXpm-3.5.12,gettext-0.19.8.1)
$(call dep,mesa-17.0.0,libx11-1.6.5)
$(call dep,glu-9.0.0,mesa-17.0.0)
$(call dep,glew-2.0.0,mesa-17.0.0)
$(call dep,freeglut-3.0.0,glu-9.0.0 cmake-3.5.2)
$(call dep,mesa-demos-8.3.0,glew-2.0.0 freeglut-3.0.0 freetype-2.7.1)
$(call dep,pixman-0.34.0,libpng-1.6.28)
$(call dep,xtrans-1.3.5,fontconfig-2.12.1)
$(call dep,xorg-server-1.19.3,pixman-0.34.0 nettle-3.2)
$(call dep,intltool-0.50.2,perl-5.24.1 expat-2.1.0)
$(call dep,xkeyboard-config-2.20,gettext-0.19.8.1 intltool-0.50.2)
$(call dep,atk-2.24,glib-2.49.6 bison-3.0.2 gettext-0.19.8.1)
$(call dep,cairo-1.14.8,glib-2.49.6 gettext-0.19.8.1)
$(call dep,pango-1.40.5,cairo-1.14.8)
$(call dep,gdk-pixbuf-2.36.6,glib-2.49.6 gettext-0.19.8.1 tiff-4.0.7)
$(call dep,gtk+-2.24.31,atk-2.24 gdk-pixbuf-2.36.6 pango-1.40.5)
$(call dep,fceux-2.2.3, gtk+-2.24.31 SDL-1.2.15 glu-9.0.0)

##
## Make all the X prelibs depend on all the X headers (if someone wants to
## go through here with a comb and determine all the individual dependencies,
## go for it).
##

$(foreach prelib,$(X_PRELIBS),$(eval $(call dep,$(prelib),$(X_HEADERS))))

##
## Make all the X libs dependent on the X prelibs.
##

$(foreach xlib,$(X_LIBS),$(eval $(call dep,$(xlib),$(X_PRELIBS))))

##
## Make all the X utilities dependent on the X libs.
##

$(foreach xutil,$(X_UTILITIES),$(eval $(call dep,$(xutil),$(X_LIBS))))

##
## Make all the X fonts and X itself dependent on the X utilities.
##

$(foreach x,$(X_FONTS) $(X),$(eval $(call dep,$(x),$(X_UTILITIES))))

##
## Finally, make all the X apps dependent on X.
##

$(foreach xapp,$(X_APPS),$(eval $(call dep,$(xapp),$(X))))

