# Peresvet OS Third Party Packages

This repository contains a collection of open source packages from around the interwebs that build on [Peresvet OS](https://gitlab.com/peresvet/os). It includes the sources for the GCC toolchain used to build the OS.

Building a package here will cross compile that package to run on Peresvet OS (or simply build the package natively if you're already running on Peresvet OS). Some packages can also be built as "tools", which means they'll compile to run on the build machine (the machine you're currently building on). Note that these tools get compiled twice: once targeting the build machine, and then another time when being cross compiled for Peresvet OS.

Some packages try to run the thing they just compiled in order to finish their build process. Scripting languages are usually the worst offenders. These packages cannot be cross compiled, and will be quietly skipped when running make. The only way to build every single package in this repository is to do it on Peresvet OS. The majority of packages, however, can be cross compiled.

Notice: Peresvet OS is a fork of Minoca OS built with seL4 kernel.

### Build Environment
This repository uses the same build environment variables described in the Peresvet OS [readme](https://gitlab.com/peresvet/os/blob/master/README.md). `SRCROOT` must be set to the parent directory of this repository, and this repository must be checked out to a directory called `third-party`. For example, if you've cloned this repository and the location of this file is ~/src/third-party/README.md, then you'd set SRCROOT by `export SRCROOT=~/src`. Other variables that need to be set include `ARCH` (set to `x86`, `armv7`, `armv6`), `VARIANT` (usually blank, set to `q` to build for Intel Quark), and `DEBUG` (set to `dbg` for debug or `rel` for release).

You'll need to have `$SRCROOT/$ARCH$VARIANT$DEBUG/tools/bin` in your PATH in order to build successfully. We recommend having it first in your path so that older versions of build tools in your system path don't conflict with the build tools created here.

To build the toolchain, you'll also need to have the [os](https://gitlab.com/peresvet/os) repository checked out to `$SRCROOT/os`. The compiler uses the os repository to gather the Peresvet OS headers and compile the C library. Once `make tools` is complete, the os repository is no longer needed to build this repository.

### Building
 * To build the minimal set of tools needed to build the `os` repository, run `make tools`. This essentially builds GCC and iASL, the only dependencies of Peresvet OS.
 * To build the set of tools needed to build the remaining packages in this repository, run `make all-tools`. For instance, to build OpenSSL you need Perl on your build machine, so Perl is built as a tool during `make all-tools`. The `all-tools` target is a superset of `tools`.
 * To build all packages and tools in this repository, simply run `make`. This will run `make all-tools` if it has not already been built.
 * To build all supported packages, just type `make`. If the toolchain has not yet been built, running this will build it.
 * Run `make clean` to clean all the tools, or simply delete `$SRCROOT/$ARCH$VARIANT$DEBUG/obj/third-party`. To completely clean everything, run `make wipe` from the os repository, or simply delete `$SRCROOT/$ARCH$VARIANT$DEBUG`.
 * To work with an individual package (rather than the repository as a whole), cd into build/<package_name>. You can run `make`, `make clean`, and `make tools` (if supported) from inside that directory to operate on just that package. There's also usually a `make recreate-diff` target for regenerating the patch file after you've made changes to the source.

Packages that support it will be built with -j enabled for parallel builds. You can set the environment variable `NO_PARALLEL_MAKE` to any value to disable the use of parallel make.

The general outline of the steps that `make` performs to build each package are as follows:
 * Untar the package into `$SRCROOT/$ARCH$VARIANT$DEBUG/obj/third-party/<package>.src/`
 * Apply the patch found at `$SRCROOT/third-party/build/<package>/<package>.diff`
 * cd into `$SRCROOT/$ARCH$VARIANT$DEBUG/obj/third-party/<package>.build/` (substitute `.build` for `.tool` if building a tool).
 * Run `configure` and `make` in the .build directory.
 * Run `make install` to install the finished files to `$SRCROOT/$ARCH$VARIANT$DEBUG/obj/third-party/<package>.build/build.out`.
 * Assemble the package contents and control information into `$SRCROOT/$ARCH$VARIANT$DEBUG/obj/third-party/<package.build>/build.pkg`.
 * Create the `.ipk` package, and place the final package in `$SRCROOT/$ARCH$VARIANT$DEBUG/bin/packages`.

There are variations to this flow for simpler packages (like awk) and more complicated packages (like GCC), but by and large most packages follow this routine.

### Building on Windows
We at Peresvet use Windows as a development platform (though this repository also successfully compiles on Ubuntu and Mac OS X). In order to build the packages in this repository on Windows, you'll need the [tools](https://gitlab.com/peresvet/tools) repository checked out to `$SRCROOT/tools`. It contains a MinGW compiler, make, a precompiled version of swiss (for sh and other standard unix utilities), and other tools. We recommend initializing the environment using the [setenv.cmd](https://gitlab.com/peresvet/tools/blob/master/win32/scripts/setenv.cmd) script. Initialize it by running `cmd.exe /k C:/path/to/tools/win32/scripts/setenv.cmd x86 dbg` (substituting your desired architecture and debug level). We create desktop shortcuts to each architecture. The build process requires a Bourne shell, trying to make directly from cmd.exe will not work.

### License
The tarballs in `./src`, and the diff files we produced for each package are licensed under the terms specified by each respective package author. The rest of the content in this repository is licensed under the GNU General Public License, version 3.

### Submissions
Got a package you want to officially add to Peresvet OS? How about a bug fix to a package or build script? Perhaps you've built a newer version of one of these packages? We would love your submissions. Send them to peresvet-dev@googlegroups.com.

### Pushing packages upstream
Ultimately we don't really want to be in the business of maintaining patches. We'd love to see these patches get submitted upstream for official inclusion. If you are the owner of one of the packages built here, or are willing to submit these changes on our behalf, then by all means, go ahead. There are a couple of things worth noting though:
 * Most patches here need at least a little work before the upstream author is likely to accept them. At the very least, many of them are missing autoconf changes, since we often simply hack on the configure scripts directly and don't always reflect those changes to the .m4 sources that built the configure script.
 * Some changes to a package are meant as temporary changes until we've implemented a feature in the OS, and aren't meant to be pushed upstream. Often these are accompanied by a comment or TODO. If you're not sure, feel free to ask.
 * Peresvet OS is a new OS, and we have a chance to do things right. If a change being pushed upstream is working around a bug or deficiency in Peresvet OS, we should at least have a discussion about whether or not we can fix this in the OS before throwing OS-specific workarounds in other packages.

### Contact
 * Email: peresvet-dev@googlegroups.com
 * Github: [https://github.com/peresvet](https://github.com/peresvet)
 * Gitlab: [https://gitlab.com/peresvet](https://gitlab.com/peresvet)
